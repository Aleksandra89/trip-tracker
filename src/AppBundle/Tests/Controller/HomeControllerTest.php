<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{
	protected $client;

	public function setUp()
	{
		$this->client = static::createClient();
	}

    public function testIndexAction()
    {
        $crawler = $this->client->request('GET', '/');

        $this->assertGreaterThan(
            0,
            $crawler->filter('html:contains("Welcome to Trip Tracker")')->count()
        );
    }

    public function testErrorAction()
    {
        $crawler = $this->client->request('GET', '/error/some-error-message');

        $this->assertGreaterThan(
            0,
            $crawler->filter('html:contains("some-error-message")')->count()
        );
    }

}
