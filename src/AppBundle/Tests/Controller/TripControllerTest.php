<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Browserkit\Cookie;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class TripControllerTest extends WebTestCase
{
	protected $client;
    protected $logged_user;

	public function setUp()
	{
        $this->client = static::createClient();

        $em = $this->client->getContainer()->get('doctrine')->getManager();
        $this->logged_user = $em->getRepository('AppBundle:User')->find(1);
        
        $this->authorizeClient();
	}

    public function testIndexAction()
    {
        $crawler = $this->client->request('GET', '/');
        $link = $crawler->selectLink('My Trips')->link();
        $crawler = $this->client->click($link);       

        $trips = $this->logged_user->getTrips();
        $no_trips = count($trips);
        $trip = $trips->first();

        $this->assertTrue($crawler->filter("th:contains('{$no_trips}')")->count() == 1);
        $this->assertTrue($crawler->filter("td:contains('{$trip->getName()}')")->count() == 1);
        $this->assertTrue($crawler->filter("a:contains('{$trip->getXmlName()}')")->count() == 1);
    }

    public function testShowAction()
    {
        $crawler = $this->client->request('GET', '/');
        $link = $crawler->selectLink('My Trips')->link();
        $crawler = $this->client->click($link);       

        $trip = $this->logged_user->getTrips()->first();

        $link = $crawler->selectLink('Show')->eq(0)->link();
        $crawler = $this->client->click($link);  

        $this->assertTrue($crawler->filter("h1:contains('{$trip->getName()}')")->count() == 1);
    }

    public function testCreateAction()
    {
        $crawler = $this->client->request('GET', '/');
        $link = $crawler->selectLink('Add Trip')->link();
        $crawler = $this->client->click($link);       

        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();

        $form['trip[name]'] = 'Test Trip';
        $form['trip[xmlFile]']->upload($this->client->getKernel()->getRootDir() . '\Resources\test\test.gpx');

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());
        $crawler = $this->client->followRedirect();
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    private function authorizeClient()
    {
        $container = $this->client->getContainer();
        $session = $container->get('session');

        /**@var $userManager \FOS\UserBundle\Doctrine\UserManager */
        $userManager = $container->get('fos_user.user_manager');
        /** @var $loginManager \FOS\UserBundle\Security\LoginManager */
        $loginManager = $container->get('fos_user.security.login_manager');
        $firewallName = $container->getParameter('fos_user.firewall_name');

        $user = $userManager->findUserBy(['username' => $this->logged_user->getUsername()]);
        $loginManager->loginUser($firewallName, $user);

        // save the login token into the session and put it in a cookie
        $container->get('session')->set('_security_' . $firewallName,
            serialize($container->get('security.context')->getToken()));
        $container->get('session')->save();
        $this->client->getCookieJar()->set(new Cookie($session->getName(), $session->getId()));
    }

}
