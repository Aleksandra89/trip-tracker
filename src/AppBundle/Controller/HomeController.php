<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends FOSRestController
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
    	$view = $this->view()
    		->setTemplate("AppBundle:Home:index.html.twig");

        return $this->handleView($view);
    }

	/**
     * @Route("/error/{message}", name="error")
     */
    public function errorAction($message = 'Oops... Something went wrong!')
    {
    	$view = $this->view()
    		->setTemplate("AppBundle:Error:index.html.twig")
    		->setTemplateData(['message' => $message]);

        return $this->handleView($view);
    }

}
