<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Trip;
use AppBundle\Form\TripType;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/trips")
 */
class TripController extends FOSRestController
{
    /**
     * List trips
     *
     * @Route ("/", name="trips_index")
     * @return Response
     */
    public function indexAction()
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($this->getUser());

        $trips = $user->getTrips();

        $view = $this->view($trips, 200)
            ->setTemplate("AppBundle:Trip:index.html.twig")
            ->setTemplateVar('trips');

        return $this->handleView($view);
    }

    /**
     * Trip details
     *
     * @Route ("/{id}", requirements={"id" = "\d+"}, name="trips_show")
     * @ParamConverter ("trip", class="AppBundle:Trip")
     * @Security ("trip.isCreator(user)")
     * @param AppBundle\Entity\Trip $trip
     * @return Response
     */
    public function showAction(Trip $trip)
    {
        $templateData = ['points' => json_decode($trip->getJsonPoints())];

        $view = $this->view($trip, 200)
            ->setTemplate("AppBundle:Trip:details.html.twig")
            ->setTemplateVar('trip')
            ->setTemplateData($templateData);

        return $this->handleView($view);
    }

    /**
     * Add a new trip 
     *
     * @Route ("/create", name="trips_create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $user = $this->getUser();

        $trip = new Trip;
        $form = $this->createForm(TripType::class, $trip);
        
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $trip->setUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($trip);

            if (!$this->validateXmlFile($trip)) {
                $view = $this->routeRedirectView(
                    'error',
                    ['message' => 'XML file not valid!']
                );

                return $this->handleView($view);
            }

            $em->flush();

            $view = $this->routeRedirectView(
                'trips_show',
                ['id' => $trip->getId()]
            );

            return $this->handleView($view);
        }

        $view = $this->view($form, 200)
            ->setTemplate("AppBundle:Trip:create.html.twig")
            ->setTemplateVar('form');

        return $this->handleView($view);
    }

    /**
     * @param \AppBundle\Entity\Trip $trip
     * @return boolean
     */
    private function validateXmlFile($trip)
    {
        $xmlContent = $this->fetchXmlContent($trip);

        // check if xmlContent has valid gpx structure
        if (!isset($xmlContent->trk->trkseg) || !isset($xmlContent->trk->trkseg->trkpt)) {
            return false;
        }

        $trip_points = $xmlContent->trk->trkseg->trkpt;

        $this->saveTripPoints($trip);

        return true;
    }

    /**
     * @param \AppBundle\Entity\Trip $trip
     */
    private function saveTripPoints($trip)
    {
        $xmlContent = $this->fetchXmlContent($trip);
        $trip->setJsonPoints(json_encode($xmlContent->trk->trkseg));
    }

    /**
     * @param \AppBundle\Entity\Trip $trip
     */
    private function fetchXmlContent($trip)
    {
        $xmlDataUrl = $trip->getXmlName();
        $xmlFileData = file_get_contents($this->get('kernel')->getRootDir() . "/../web/xml/trips/$xmlDataUrl");
        return simplexml_load_string($xmlFileData);
    }

}
