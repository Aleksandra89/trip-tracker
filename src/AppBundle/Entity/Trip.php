<?php

namespace AppBundle\Entity;

use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @Vich\Uploadable
 * @ORM\Table(name="trips")
 */
class Trip
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=100)
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="trips")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", length=65535)
     */
    protected $json_points = '{}';

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property
     *
     * @Vich\UploadableField(mapping="trip_xml", fileNameProperty="xmlName")
     *
     * @Assert\File(
     *     maxSize = "1024k",
     *     mimeTypes = {"application/xml"},
     *     mimeTypesMessage = "Please upload a valid GPX"
     * )
     *
     * @var File
     */
    protected $xmlFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    protected $xmlName;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Trip
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Trip
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return File
     */
    public function getXmlFile()
    {
        return $this->xmlFile;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $xml
     */
    public function setXmlFile(File $xml = null)
    {
        $this->xmlFile = $xml;

        if ($xml) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return string|null
     */
    public function getXmlName()
    {
        return $this->xmlName;
    }

    /**
     * @param string $xmlName
     * @return Trip
     */
    public function setXmlName($xmlName)
    {
        $this->xmlName = $xmlName;

        return $this;
    }

    /**
     * Get json points
     *
     * @return string
     */
    public function getJsonPoints()
    {
        return $this->json_points;
    }

    /**
     * Set json points
     *
     * @param string $json_points
     * @return Trip
     */
    public function setJsonPoints($json_points)
    {
        $this->json_points = $json_points;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Trip
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Check if given user is creator of trip
     *
     * @param AppBundle\Entity\User $user
     * @return bool
     */
    public function isCreator(User $user = null)
    {
        return $user && $user->getId() == $this->getUser()->getId();
    }

}
